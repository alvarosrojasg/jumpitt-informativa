


<h1>Ventas clientes ...</h1>

<h2> Por qué asociarse con Jumpitt  </h2>


<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <div class="">
        
                <ul>
                    <li>Aumentaran las ventas mes a mes.</li>
                    <li>Obtendrán una plataforma de publicidad personalizada, especial para estrategias de marketing segmentadas por edad, 
                    historial de compras, geo-posición, etc. Podrán realizar ofertas y promociones como nunca antes.</li>
                    <li>Una total transparencia contable, ya que en tiempo real podrán ver lo que sus consumidores están comprando, 
                    formas de pago y utilización de cupones de descuento, por ejemplo. </li>
                    <li>Podrán promocionar su negocio de una forma totalmente nueva y efectiva.</li>
                    <li>Seguridad total en cada transacción, ya que ellos no manejaran el proceso de pago, 
                     esto estará a cargo de JUMPITT, y ellos tranquilamente recibirán el total vendido.</li>
                    <li>Liberación de horas hombre a largo plazo (cajeros) que no agregan valor, para poder destinarla a la cocina por ejemplo. <br> 
                    <li>Optimización de su personal y proceso productivo, al saber con mayor precisión y antelación los futuros pedidos.</li>
                    <li>Se unirán a la revolución móvil, sin la necesidad de una gran inversión en desarrollo, diseño y mantención de aplicaciones. </li>
                    <li>Comunicación directa con sus consumidores, además de la directa retroalimentación que estos le darán a través las redes sociales. </li>

                </ul>
            </div>
            
        </div>
        
    </div>
</div>
    

    
