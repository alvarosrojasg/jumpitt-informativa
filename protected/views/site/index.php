<div id="contenido1" class="contenedor">
     
    <br><br>
                    
                    </div>
                    <!-- contenido de intro  -->
         
                    <div class="container-fluid">
                        <div class="row-fluid">
                            
                            
                            
                            <div class="span6">
                                <div class="">
                                
                                    <div class="capitalize titulo">¿Qué es Jumpitt?</div>
                    
                                    
                                    <h4>La aplicación para ganar Tiempo</h4>
                              
                                    <div class="parrafo">
                                    Jumpitt es la primera aplicación de mobile-commerce en Latinoamérica, 
                                    que te permitirá optimizar tu tiempo al comprar tu comida rápida favorita, 
                                    y miles de otros productos sin perder tiempo esperando en una fila.
                                    </div>    
                                    
                                </div>
                            </div>
                            
                            <div class="span6">
                                <div class=" alinear">
                                
                                    <img class="img-rounded foto_que_es_jumpit_top" src="<?php echo Yii:: app() ->baseUrl.'/images/fila2_200.jpg' ?>"/>
                                    
                                </div>       
                            </div>
                            
                        </div>
                    </div>
                        
                    
                    <!-- final de contenido intro -->
                    <div class="borde_limite"></div>

                    
        <div id="contenido2" class="contenedor">
                    <div class="capitalize titulo">¿Cómo funciona?<a href="javascript:$.scrollTo('#inicio',800);" ><div class="pull-right"><img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/icon/1_flecha.png' ?>"/></div></a>
                     </div>
                    
                    
                    
                     
                     <div class="container-fluid">
                        <div class="row-fluid"> 
                            
                           
                            
                            <div class="span4">
                                
                                <div class="alinear">
                                    <img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/paso1_172x161.png' ?>"/><br>
                                    
                                    <h4>Descubre los locales cerca de ti</h4>
                                    
                                    <div class="parrafo">
                                    Puedes filtrar la busqueda por ciudad, categoría de local o tipo de comida que buscas
                                    </div>
                                </div>   
                                
                            </div>
                            
                             <div class="span4">
                                
                                 <div class="alinear">
                                    
                                    <img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/paso2_172x161.png' ?>"/><br>
                                    <h4>Haz tu pedido con solo deslizar el dedo.</h4>
                                    
                                    <div class="parrafo">
                                    Revisa con calma el menú de tu restaurant favorito o los productos y servicios que 
                                    están esperando por ti.
                                    </div>
                                    
                                 </div>   
                              </div>
                            
                             <div class="span4">
                                 <div class="alinear">
                                    
                                    <img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/paso3_172x161.png' ?>"/><br>
                                    <h4>Realiza un pago sin esperar un cajero.</h4>
                                    
                                    <div class="parrafo">
                                    Cuando tengas listo tu pedido, págalo directamente desde Jumpitt.
                                    Es totalmente seguro con el sistema WebPay.
                                    </div>
                                    
                                 </div>   
                            </div>
                            
                        </div>
                    </div>
                    
        </div>
                    
                    <div class="borde_limite"></div>
                    
        <div id="contenido3" class="contenedor">
                    <div class="capitalize titulo">¿Por qué Jumpitt?<a href="javascript:$.scrollTo('#inicio',800);" ><div class="flecha pull-right"><img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/icon/1_flecha.png' ?>"/></div></a>
                    </div>
                    
        </div>
                    <!-- inicio contenido productos -->
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="span4">
                                <div class="">
                                    
                                    <img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/1_socios_172x230.jpg' ?>"/>
                                    
                                    <img class="img-rounded margin_socio_top" src="<?php echo Yii:: app() ->baseUrl.'/images/2_socios_172x230.jpg' ?>"/>
                               
                                </div>
                                
                            </div>
                            
                            <div class="span8">
                                <div class="">
                                
                                    
                                    <div class="parrafo">
                                        
                                        <img src="<?php echo Yii:: app() ->baseUrl.'/images/icon/VistoBueno_15.jpg' ?>"/>
                                        Ordena desde cualquier lugar, antes de llegar al centro comercial, o cómodamente sentado desde tu mesa en el patio de comidas.
                                        <br><br>
                                        <img src="<?php echo Yii:: app() ->baseUrl.'/images/icon/VistoBueno_15.jpg' ?>"/>
                                        Utiliza tu Smartphone, Tablet o notebook.
                                        <br><br>
                                        <img src="<?php echo Yii:: app() ->baseUrl.'/images/icon/VistoBueno_15.jpg' ?>"/>
                                        No harás mas filas, ni para pedir, pagar o esperar que este preparada tu orden. 
                                        <br><br>
                                        <img src="<?php echo Yii:: app() ->baseUrl.'/images/icon/VistoBueno_15.jpg' ?>"/>
                                        Seguridad en tus transacciones e historial de estas para tu tranquilidad.
                                        <br><br>
                                        <img src="<?php echo Yii:: app() ->baseUrl.'/images/icon/VistoBueno_15.jpg' ?>"/>
                                        Guarda tu locales favoritos y realiza tu pedidos frecuentes con un solo toque.
                                        <br><br>
                                        <img src="<?php echo Yii:: app() ->baseUrl.'/images/icon/VistoBueno_15.jpg' ?>"/>
                                        Recibe ofertas, cupones y bonificaciones directamente en tu teléfono celular.
                                        <br><br>
                                        <img src="<?php echo Yii:: app() ->baseUrl.'/images/icon/VistoBueno_15.jpg' ?>"/>
                                        Descubre nuevos lugares, comenta y comparte con amigos tus fotos y experiencias.
                                        <br><br>
                                        <img src="<?php echo Yii:: app() ->baseUrl.'/images/icon/VistoBueno_15.jpg' ?>"/>
                                        Lo mejor de todo, que es totalmente gratis.

                                    </div>
                                </div>   
                            </div>
                        </div>
                    </div>
                    
                    <!-- final de contenido producto -->
                    
                    <div class="borde_limite"></div>
                    
        <div id="contenido5" class="contenedor">
                    <div class="capitalize titulo">Nos apoyan <a href="javascript:$.scrollTo('#inicio',800);" ><div class="flecha pull-right"><img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/icon/1_flecha.png' ?>"/></div></a>
                    </div>
                  
                   
                    
        </div>
                    <!-- inicio contenido contacto -->

                    <div class="container-fluid">
                        <div class="row-fluid">
                            
                                <div class="span3">
                                <div class="alinear">
                                    <a href="http://www.corfo.cl" target="_blank"><img src="<?php echo Yii:: app() ->baseUrl.'/images/apoyo/logoinnova_42.jpg' ?>"/></a>
                                </div> 
                            </div>
                             <div class="span3">
                                 <div class="alinear">
                                     <a href="http://www.chrysalis.cl" target="_blank"> <img src="<?php echo Yii:: app() ->baseUrl.'/images/apoyo/logochrysalis_42.jpg' ?>"/></a>
                                 </div>
                            </div>
                             <div class="span3">
                                 <div class="alinear">
                                     <a href="http://eii.ucv.cl" target="_blank"><img src="<?php echo Yii:: app() ->baseUrl.'/images/apoyo/logoeii_42.jpg' ?>"/></a>
                                 </div>
                            </div>
                            
                             <div class="span3">
                                 <div class="alinear">
                                 
                                     <a href="http://www.pucv.cl" target="_blank"><img src="<?php echo Yii:: app() ->baseUrl.'/images/apoyo/logopucv_42.jpg' ?>"/></a>
                                 </div>
                             </div>
                            
                                   
                        </div>
                        
                    </div>    
                   
                    <div class="borde_limite"></div>
                    
                   
                    
                    
                 
                    
                    
                    <!-- final contenido contacto -->

