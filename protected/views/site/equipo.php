
<div class="container-fluid">
<div class="row-fluid"> 

<div class="span6">
    
    <h2>Quienes Somos</h2>
    
    <div class="justificar sangria">
        
        Somos un equipo joven y multidisciplinario, comprometidos con nuestra misión: 
        Proveer un servicio de calidad para que la gente optimice su tiempo y los locales 
        lleguen de mejor manera a sus consumidores.

    </div>
    
</div>

<div class="span6">
    
  
      
</div>

</div>
</div>   


<br>



<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3">
            <div class=" alinear">
            
                <img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/equipo/marcos_154x200.jpg' ?>"/><br>
                Marcos Amador<br>
                CEO/Founder
                
            </div>
        </div>
        
         <div class="span3">
            <div class=" alinear">
                
                <img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/equipo/felipe_154x200.jpg' ?>"/><br>
                
                Felipe Ojeda<br>
                COO/Co-Founder
                
            </div>
            
        </div>
        
         <div class="span3">
            <div class=" alinear">
                
                <img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/equipo/belen_154x200.jpg' ?>"/><br>
                María Belén Gomez<br>
                CLO
                
            </div>
            
        </div>
        
         <div class="span3">
            <div class=" alinear">
                
                <img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/equipo/alvaro_154x200.jpg' ?>"/><br>
                Alvaro Rojas<br>
                CTO
                
            </div>
            
        </div>
        
    </div>
</div>

<br><br>

<div class="container-fluid">
    <div class="row-fluid">
       
        
         <div class="span3">
            <div class=" alinear">
                
                <img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/equipo/rodrigo_154x200.jpg' ?>"/><br>
                
                Rodrigo Urbina<br>
                Web Master
                
            </div>
            
        </div>
        
         <div class="span3">
            <div class=" alinear">
                
                <img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/equipo/olga_154x200.jpg' ?>"/><br>
                Olga Orrego<br>
                UX-UI
                
            </div>
            
        </div>
        
        <div class="span3">
            <div class=" alinear">
                
                <img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/equipo/matias_154x200.jpg' ?>"/><br>
                Matías Arancibia<br> 
                Android Developer
                
            </div>
            
        </div>
        
        <div class="span3">
            <div class=" alinear">
                
                 <img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/equipo/alexis_154x200.jpg' ?>"/><br>
                Alexis Abarca<br>
                Android Developer
                
            </div>
            
        </div>
        
    </div>
</div>

<br><br>

<div class="container-fluid">
    <div class="row-fluid">
        
       
        
         <div class="span12">
            <div class="alinear">
                
                <img class="img-rounded" src="<?php echo Yii:: app() ->baseUrl.'/images/equipo/camilo_154x200.jpg' ?>"/><br>
                Camilo Castro<br>
                IOS Developer
                
            </div>
            
        </div>
            
    </div>
</div>

<div class="borde_limite"></div>